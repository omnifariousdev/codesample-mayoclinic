/**
 * This code is an unmodified clone (with possible whitespace changes) of code written on contract by Daniel J. Post Consulting, Inc for Mayo Clinic in 2012 and 2013. At this writing the original source is available at https://www.mayo.edu/styles/js/gbs/app-v2.js
 * Copied here based on a good faith assumption of "fair use" - there is nothing proprietary about this code, and the code is readable by anyone with access to the mayo.edu website, as of this writing.
 * @author Daniel J. Post <geek@danieljpost.info>
 *
 */
// BEHAVIORS THAT CAN HAPPEN AS SOON AS THIS FILE IS PARSED.
// re-runs resizeMenus on window resize.
$(window).resize(MayoApp.resizeMenus);
// moves some content from "above the fold" to "below the fold" in mobile view.
$(window).resize(MayoApp.moveAuto);

// BEHAVIORS THAT CAN HAPPEN ON DOCUMENT.READY
$(function() {
  MayoApp.moveAuto();
  $(".slide-show").slide_show();
  if ($(".enlarge-image").length > 0) {
    $(".enlarge-image").slide_show_enlarge();
  }
  if ($(".show-more").length > 0) {
    $(".show-more").show_more();
  }

  // FS-247 default namespace with CSS-only behavior, closed by default
  if ($(".expandable").length > 0) {
    $(".expandable").toggle_open();
  }

  // FS-255 et al.
  ($holder = $("#filter").find(".holder")),
    ($pull = $(".mobilePull")),
    ($menu = $(".topnav-v2"));

  var toggleNext = function(e) {
    "use strict";
    e.preventDefault();
    e.stopPropagation();
    // don't care if link is next to <div>, <ul> or <ol>
    $(this)
      .next()
      .toggle();
    return this;
  };

  $pull.on("click.pull", toggleNext);

  // add an extra anchor to search results pages if page is mobile.
  $("#filter")
    .find(".holder")
    .each(function() {
      "use strict";
      // add an anchor before the item for JavaScript to make clickable.
      var $this = $(this);
      var $text = $this.find("h2").text();
      var $a = $("<a>")
        .attr("href", "#")
        .html($text)
        .addClass("mobilePull");
      $a.on("click.pull", toggleNext);
      $this.before($a);
    });

  // FS-244. @author Daniel J. Post
  // show forms if they contain errors
  var $forms = $(".form");
  $forms.each(function(i, f) {
    "use strict";
    var $f = $(f);
    if ($f.find(".error").length) {
      $f.find(".popup")
        .addClass("show")
        .find("h2")
        .eq(0)
        .focus();
    }
  });
  // show/hide "email this article" or similar form
  $forms.find(".showpopup").on("click.popup", function(e) {
    "use strict";
    e.preventDefault();
    $(this)
      .closest(".form")
      .find(".popup")
      .toggleClass("show");
  });
  // hides all .form .popup.show
  var closeE = function() {
    "use strict";
    $forms.each(function(i, f) {
      var $f = $(f);
      $f.find(".popup").removeClass("show");
    });
  };
  // capture <ESC> key.
  $(document).on("keyup.popup", function(e) {
    "use strict";
    if (e.keyCode === 27) {
      closeE();
    }
  });
  // hide forms when click [x]
  $(".closeform").on("click.popup", closeE);

  $newsticker = $(".news-ticker");
  $newsticker_content = $newsticker.find("li");
  if ($newsticker_content.length) {
    // do all news tickers on the page with default behavior
    $newsticker.newsticker();
  }

  // Keyboard navigation for top nav - tabbing
  $("nav.resizable ul.topnav-v2 > li > a").focus(function() {
    $(this)
      .parent("li")
      .siblings()
      .find("div")
      .removeClass("on");
    $(this)
      .next("div")
      .addClass("on");
  });
  $("nav.resizable ul.topnav-v2 a:last").blur(function() {
    $("nav.resizable ul.topnav-v2 div").removeClass("on");
  });

  // add data-title attributes to td elements, to be displayed in mobile view.
  $("table.standard").each(function() {
    "use strict";
    var th = [],
      $table = $(this),
      $tr = $table.find("tr");
    $table.find("th").each(function() {
      th.push(this.innerHTML);
    });
    $tr.each(function() {
      $(this)
        .find("td")
        .each(function(i, j) {
          $(j).attr("data-title", th[i]);
        });
    });
  });
  // If IE8 and after page load, fix heights on slide-show initial images [FS-210]
  if (!MayoApp.isModern) {
    $slideshow = $(".slide-show");
    $slideshow.each(function(i, el) {
      var $el = $(el),
        $img = $el.find(".slide").find("img");
      if ($img.height()) {
        var ratio = $img.height() / $img.width();
        var displayHeight = $el.width() * ratio;
        $img
          .height(displayHeight)
          .closest("div")
          .height(displayHeight);
      }
    });
  }
});
