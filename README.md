# About this Repository

This is sample of the JavaScript and other work done by Daniel J. Post Consulting, Inc for Mayo Clinic in 2012 and 2013.

Includes considerations for

- responsiveness
- accessibility.

Includes jQuery plugin and app code to support FS-210, FS-227, FS-244, FS-251, FS-247, FS-251

A few selected screenshots of my code in action.

### Symptoms page, fullscreen

![symptoms page, fullscreen](symptoms-fullscreen.png)

### Symptoms page, fullscreen, with the letter "Y" selected

![symptoms page, fullscreen, with the letter "Y" selected](symptoms-fullscreen-y-selected.png)

### Symptoms page, medium

![symptoms page, medium](symptoms-medium.png)

### Symptoms page, medium, with the letter "Y" selected

![symptoms page, medium, with the letter "Y" selected](symptoms-medium-y-selected.png)

### Symptoms page, narrow

![symptoms page, narrow](symptoms-narrow.png)

### Symptoms page, narrow, with the letter "Y" selected

![symptoms page, narrow, with the letter "Y" selected](symptoms-narrow-y-selected.png)
